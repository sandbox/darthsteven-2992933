<?php

namespace Drupal\entity_decorator\Traits\Core\Config\Entity;

trait ThirdPartySettingsTrait {

  /**
   * Sets the value of a third-party setting.
   *
   * @param string $module
   *   The module providing the third-party setting.
   * @param string $key
   *   The setting name.
   * @param mixed $value
   *   The setting value.
   *
   * @return $this
   */
  public function setThirdPartySetting($module, $key, $value){
    return $this->subject->setThirdPartySetting($module, $key, $value);
  }

  /**
   * Gets the value of a third-party setting.
   *
   * @param string $module
   *   The module providing the third-party setting.
   * @param string $key
   *   The setting name.
   * @param mixed $default
   *   The default value
   *
   * @return mixed
   *   The value.
   */
  public function getThirdPartySetting($module, $key, $default = NULL){
    return $this->subject->getThirdPartySetting($module, $key, $default);
  }


  /**
   * Gets all third-party settings of a given module.
   *
   * @param string $module
   *   The module providing the third-party settings.
   *
   * @return array
   *   An array of key-value pairs.
   */
  public function getThirdPartySettings($module){
    return $this->subject->getThirdPartySettings($module);
  }

  /**
   * Unsets a third-party setting.
   *
   * @param string $module
   *   The module providing the third-party setting.
   * @param string $key
   *   The setting name.
   *
   * @return mixed
   *   The value.
   */
  public function unsetThirdPartySetting($module, $key){
    return $this->subject->unsetThirdPartySetting($module, $key);
  }

  /**
   * Gets the list of third parties that store information.
   *
   * @return array
   *   The list of third parties.
   */
  public function getThirdPartyProviders(){
    return $this->subject->getThirdPartyProviders();
  }
}