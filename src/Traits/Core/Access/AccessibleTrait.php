<?php

namespace Drupal\entity_decorator\Traits\Core\Access;

use Drupal\Core\Session\AccountInterface;

trait AccessibleTrait {
  public function access($operation, AccountInterface $account = NULL, $return_as_object = FALSE) {
    return $this->subject->access($operation, $account, $return_as_object);
  }
}
