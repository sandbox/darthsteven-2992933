<?php

namespace Drupal\entity_decorator\Traits\Core\Plugin;

trait ObjectWithPluginCollectionTrait {
  /**
   * Gets the plugin collections used by this object.
   *
   * @return \Drupal\Component\Plugin\LazyPluginCollection[]
   *   An array of plugin collections, keyed by the property name they use to
   *   store their configuration.
   */
  public function getPluginCollections() {
    return $this->subject->getPluginCollections();
  }
}