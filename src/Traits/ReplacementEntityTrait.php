<?php

namespace Drupal\entity_decorator\Traits;

trait ReplacementEntityTrait {

  /**
   * @inheritDoc
   */
  public function __construct(array $values, $entity_type) {
    $class = $this->_getReplacedClassName();
    if (empty($class) || !class_exists($class)) {
      throw new \Exception('@TODO: use a proper exception');
    }

    $subject = new $class($values, $entity_type);

    /** @var \Drupal\entity_decorator\Entity\EntityDecoratorManager $plugin_manager */
    $plugin_manager = \Drupal::service('plugin.manager.entity_decorator.entity_decorator');
    $plugins = $plugin_manager->getDecoratorDefinitions($entity_type);
    foreach ($plugins as $plugin_id => $plugin) {
      $subject = $plugin_manager->createInstance($plugin_id, ['subject' => $subject]);
    }

    $this->subject = $subject;
  }

}