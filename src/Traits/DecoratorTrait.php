<?php

namespace Drupal\entity_decorator\Traits;

use Drupal\Core\Entity\EntityInterface;

trait DecoratorTrait {

  /**
   * The decorated subject.
   */
  protected $subject;

  /**
   * @inheritDoc
   */
  public function __construct(EntityInterface $subject) {
    $this->subject = $subject;
  }

  /**
   * Returns true if $method is a PHP callable.
   *
   * @param string $method
   *   The method name.
   * @param bool $checkSelf
   *
   * @return bool|mixed
   */
  public function isCallable($method) {
    if (is_callable([$this->subject, $method])) {
      return $this->subject;
    }
    return FALSE;
  }

  /**
   * @param $method
   * @param $args
   *
   * @return mixed
   *
   * @throws \Exception
   */
  public function __call($method, $args) {
    if ($object = $this->isCallable($method)) {
      return call_user_func_array([$object, $method], $args);
    }
    throw new \Exception(
      'Undefined method - ' . get_class($this->subject) . '::' . $method
    );
  }

  /**
   * @param $property
   *
   * @return null
   */
  public function __get($property) {
    if (property_exists($this->subject, $property)) {
      return $this->subject->$property;
    }
    return NULL;
  }

  /**
   * @param $property
   *
   * @return null
   */
  public function __set($property, $value) {
    if (property_exists($this->subject, $property)) {
      return $this->subject->$property = $value;
    }
    return NULL;
  }

}