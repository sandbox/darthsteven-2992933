<?php

namespace Drupal\entity_decorator\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines an entity decorator annotation object.
 *
 * Plugin Namespace: Plugin\EntityDecorator.
 *
 * @Annotation
 */
class EntityDecorator extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The Entity type id the plugin decorates.
   *
   * @var string
   */
  public $entity_type_id;

  /**
   * Priority.
   */
  public $priority = 0;
}
