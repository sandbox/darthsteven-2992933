<?php

namespace Drupal\entity_decorator\Entity\Replacements;

use Drupal\entity_decorator\Entity\Decorators\ImageStyleDecoratorBase;
use Drupal\entity_decorator\Entity\ReplacementEntityInterface;
use Drupal\entity_decorator\Traits\ReplacementEntityTrait;

final class ImageStyle extends ImageStyleDecoratorBase implements ReplacementEntityInterface {
  use ReplacementEntityTrait;

  public function _getReplacedClassName() {
    return 'Drupal\image\Entity\ImageStyle';
  }

}
