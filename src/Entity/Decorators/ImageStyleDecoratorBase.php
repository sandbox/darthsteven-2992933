<?php

namespace Drupal\entity_decorator\Entity\Decorators;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\entity_decorator\Traits\Core\Plugin\ObjectWithPluginCollectionTrait;
use Drupal\entity_decorator\Traits\DecoratorTrait;
use Drupal\entity_decorator\Traits\image\ImageStyleTrait;
use Drupal\image\ImageStyleInterface;
use Drupal\Core\Entity\EntityWithPluginCollectionInterface;
use Drupal\image\Entity\ImageStyle as RootImageStyle;

abstract class ImageStyleDecoratorBase implements ImageStyleInterface, EntityWithPluginCollectionInterface {
  use ImageStyleTrait, ObjectWithPluginCollectionTrait, DecoratorTrait;

  /**
   * @inheritDoc
   */
  public static function load($id) {
    return RootImageStyle::load($id);
  }

  /**
   * @inheritDoc
   */
  public static function loadMultiple(array $ids = NULL) {
    return RootImageStyle::loadMultiple($ids);
  }

  /**
   * @inheritDoc
   */
  public static function create(array $values = []) {
    return RootImageStyle::create($values);
  }

  /**
   * @inheritDoc
   */
  public static function preCreate(EntityStorageInterface $storage, array &$values) {
    RootImageStyle::preCreate($storage, $values);
  }

  /**
   * @inheritDoc
   */
  public static function preDelete(EntityStorageInterface $storage, array $entities) {
    RootImageStyle::preDelete($storage, $entities);
  }

  /**
   * @inheritDoc
   */
  public static function postDelete(EntityStorageInterface $storage, array $entities) {
    RootImageStyle::postDelete($storage, $entities);
  }

  /**
   * @inheritDoc
   */
  public static function postLoad(EntityStorageInterface $storage, array &$entities) {
    RootImageStyle::postLoad($storage, $entities);
  }

}
