<?php

namespace Drupal\entity_decorator\Entity;

use Drupal\Component\Plugin\Factory\ReflectionFactory;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Manages image optimize processor plugins.
 *
 * @see hook_imageapi_optimize_processor_info_alter()
 * @see \Drupal\imageapi_optimize\Annotation\ImageAPIOptimizeProcessor
 * @see \Drupal\imageapi_optimize\ConfigurableImageAPIOptimizeProcessorInterface
 * @see \Drupal\imageapi_optimize\ConfigurableImageAPIOptimizeProcessorBase
 * @see \Drupal\imageapi_optimize\ImageAPIOptimizeProcessorInterface
 * @see \Drupal\imageapi_optimize\ImageAPIOptimizeProcessorBase
 * @see plugin_api
 */
class EntityDecoratorManager extends DefaultPluginManager {

  /**
   * Constructs a new EntityDecoratorManager.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/EntityDecorator', $namespaces, $module_handler, 'Drupal\entity_decorator\Entity\EntityDecoratorInterface', 'Drupal\entity_decorator\Annotation\EntityDecorator');

    $this->setCacheBackend($cache_backend, 'entity_decorator_plugins');
    // @TODO: Is this reflection factory very slow?
    $this->factory = new ReflectionFactory($this, $this->pluginInterface);
  }

  public function getDecoratorDefinitions($entity_type_id) {
    $matchingDefinitions = [];
    foreach ($this->getDefinitions() as $id => $definition) {
      if ($definition['entity_type_id'] == $entity_type_id) {
        $matchingDefinitions[$id] = $definition;
      }
    }
    // @TODO: Sort by priority.
    return $matchingDefinitions;
  }


}
