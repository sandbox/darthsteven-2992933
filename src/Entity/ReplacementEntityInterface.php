<?php

namespace Drupal\entity_decorator\Entity;


interface ReplacementEntityInterface {
  public function _getReplacedClassName();
}